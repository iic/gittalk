# Install 

## git
[DOWNLOAD](https://github.com/git-for-windows/git/releases/download/v2.6.1.windows.1/Git-2.6.1-64-bit.exe)

## Notepad++
[DOWNLOAD](https://notepad-plus-plus.org/repository/6.x/6.8.3/npp.6.8.3.Installer.exe)

```sh
git config --global core.editor "'C:/Program Files (x86)/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
```

## Config

```sh
git config --global user.name "yourName"
git config --global user.email "yourEmail"
git config --global http.sslVerify false
```

## TEST
테스트입니다.

